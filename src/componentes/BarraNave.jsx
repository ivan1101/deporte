import {Button} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import "./BarraNave.css";

export default function BarraNave(){
    let buttons=(

        <div>
            <Button href="/deportes" className="ms-4 mt-3 mb-3" variant="danger">Deportes</Button>
            <Button href="/noticias" className="ms-4 mt-3 mb-3" variant="danger">Noticias</Button>
            <Button href="/blog" className="ms-4 mt-3 mb-3" variant="danger">Personajes</Button>
            
            
        </div>


    )
    return(
        <header>
            <nav>
                <ul>{buttons}</ul>
            </nav>
        </header>
    )
}