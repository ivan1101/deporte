import BarraNave from "./BarraNave";
import {Button} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route } from "react-router-dom"


const estilosParrafo = {
    color:'#141212',
    backgroundColor:'#da1212',
    border:'1px solid black',
    textAlign:'center'

}

const estilosFinal={
    backgroundColor: "#FF4066",
    height: "auto",
    color:"#FFF16A",
    position: "absolute",
    bottom: "0",
    left: "0",
    width: "100%",
    textAlign: "center"
}


export const Error=()=>{

    return(
        <>
            <BarraNave/>
                <div style={estilosParrafo}>
                    <h1>ESTA PAGINA NO EXISTE YA QUE HA OCURRIDO UN ERROR AL INTRODUCIR EL LINK</h1>
                    <Button href="/" className="ms-4 mt-3 mb-3" variant="warning">VOLVER A INICIO</Button>
                    <footer class="footer" style={estilosFinal}>
                        <p>Ivi</p>
                    </footer>
                    </div>
                
        </>
    )
};