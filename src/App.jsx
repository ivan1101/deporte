import logo from './logo.svg';
import './App.css';
import BarraNave from './componentes/BarraNave';
import { BrowserRouter,Route,Routes } from "react-router-dom"
import { Deportes } from './componentes/Deportes';
import { Noticias } from './componentes/Noticias';
import { Blog } from './componentes/Blog';
import { Inicio } from './componentes/inicio';
import { Error } from './componentes/Error';
import {Footer} from './componentes/footer';





function App() {
  return (
    <>
    <BrowserRouter>
    <div>
      <Routes>
        <Route path="/" element={<Inicio/>}></Route>
        <Route path="/deportes" element={<Deportes/>}/>
        <Route path="/noticias" element={<Noticias/>}/>
        <Route path="/blog" element={<Blog/>}/>
        <Route path="*" element={<Error/>}/>
      </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
